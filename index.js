 import express, { json } from "express"
import { firstRouter } from "./src/Route/firstrouter.js";
// import { traineesRouter } from "./src/Route/trainess.js";
import { schoolRouter } from "./src/Route/schoolRouter.js";
import { traineesRouter } from "./src/Route/traineesRouter.js";
import { vehiclesRouter } from "./src/Route/vehiclesRouter.js";
import { connect } from "mongoose";
import { connectToMongoDb } from "./src/connectToDb/connecttomongoDb.js";
import { studentsRouter } from "./src/Route/studentrouter.js";
import { classRoomRouter } from "./src/Route/classRoomRouter.js";
import { departmentRouter } from "./src/Route/departmentRouter.js";
import { TeacherRouter } from "./src/Route/teacherRouter.js";
import { BookRouter } from "./src/Route/bookRouter.js";
import { CollegeRouter } from "./src/Route/collegeRouter.js";
import { contactRouter } from "./src/Route/contactRouter.js";
import { productRouter } from "./src/Route/productRouter.js";
import bcrypt from "bcrypt"
import { userRouter } from "./src/Route/userRouter.js";
import { reviewRouter } from "./src/Route/reviewRouter.js";
import jwt from "jsonwebtoken"
import { config } from "dotenv";
import { port, secretKey } from "./src/constant.js";
import { WebUserRouter } from "./src/Route/webUserRouter.js";
import { fileRouter } from "./src/Route/fileRouter.js";
config()



let expressApp = express()
expressApp.use(express.static("./public"))
expressApp.use(json())
connectToMongoDb()
// expressApp.use((req,res,next)=>{
//     console.log( "i am middleware")
//     next()
// })


//localhost8000/file
// method post
    



expressApp.use("/bike",firstRouter)
expressApp.use("/trainees",traineesRouter)
expressApp.use("/school",schoolRouter)
expressApp.use("/vehicles",vehiclesRouter)
expressApp.use("/students",studentsRouter)
expressApp.use("/classRooms",classRoomRouter)
expressApp.use("/departments",departmentRouter)
expressApp.use("/teachers",TeacherRouter)
expressApp.use("/books",BookRouter)
expressApp.use("/books",BookRouter)
expressApp.use("/colleges",CollegeRouter)
expressApp.use("/contact",contactRouter)
expressApp.use("/product",productRouter)
expressApp.use("/users",userRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/web-users",WebUserRouter)
expressApp.use("/files",fileRouter)







expressApp.listen(port, () =>{
    console.log("app is listening at port 8000");




   




    

})
//  let password ="abc@1234"
//  let hashpassword = await bcrypt.hash(password,10)
//  console.log(hashpassword);


// let  hashPassword ="$2b$10$nMu.vZySeuJPrg6T6pU3Meic52l0uMq.IVxDG.5rzATDvH3XCerX2"

// let password ="abc@1234"
// let isPasswordMatch = await bcrypt.compare(password,hashPassword)
// console.log(isPasswordMatch);




//generate token 
 let infoObj ={
    // _id:"1234456"
}
 
 let expirifyInfo ={
    expiresIn:"365d",

 }

 let token = jwt.sign(infoObj,secretKey,expirifyInfo)
// console.log(token);



//validate token
// let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoicHJhbml0YSIsImFnZSI6MjIsImlhdCI6MTcwNjM0MTQzMSwiZXhwIjoxNzM3ODc3NDMxfQ.AIvbjA0V69azHRn8K79zn1etHvdKdRys2A8dELM-LJA"
// try {
//     let infoObj = jwt.verify(token,"dw11")
//     console.log(infoObj);
// } catch (error) {
//     console.log(error.message);
    
// }




//


//for token to be validate
//token must be made from the given secret key -
//token must not expire
//if token is valid it given infoObj else error

// // make backend application (using express)
// // attached port to that application

// // url : "localhost:8000"
