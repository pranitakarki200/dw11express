import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendMail.js"
export let createUser = async(req,res,next)=>{
    let data = req.body
    let password = data.password
    let hashPassword = await bcrypt.hash(password, 10)
    data.password = hashPassword
    try {
        let result = await User.create(data)
        
sendEmail({
    from:"pranita <pranitakarki@200gmail.com>",
    to:data.email,
    subject:"Register successfully",
    html:`
    <div>
    <p>you have successfully registered in our system.</p>
    </div>
    `
})

        res.json({
            success:true,
            message:"User created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let readUser = async(req,res,next)=>{
    try {
        let result = await User.find({})
        res.json({
            success:true,
            message:"User read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let readSpecificUser = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await User.findById(id)
        res.json({
            success:true,
            message:"User read specific successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let updateUser = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result = await User.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"User updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let deleteUser = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await User.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"doesn't exist"
            })
        } else {
            res.json({
                success:true,
                message:"User deleted successfully",
            })
        }
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}







