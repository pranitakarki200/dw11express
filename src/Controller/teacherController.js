import { Teacher } from "../schema/model.js"


 export let createTeacher=async(req,res,next)=>{
    let data=req.body
    try {
        let result =await Teacher.create(data)
        res.json({
            success: true,
            message:"Teacher create successfully",
            result:result,

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
    

    
}


export let readTeacher=async(req,res,next)=>{

    let data=req.body
    try {
        let result= await Teacher.find({})
res.json({
    success:true,
    message:"Teacher Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let readSpecificTeacher=async(req,res,next)=>{
let id = req.params.id
    try {
        let result= await Teacher.findById(id)
res.json({
    success:true,
    message:"Teacher Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let UpdateTeacher=async(req,res,next)=>{

    let id =req.params.id


    let data=req.body
    try {
        let result= await Teacher.findByIdAndUpdate(id,data,{new:true})
res.json({
    success:true,
    message:"Teacher Updated successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

 export let deleteTeacher = (req,res,next)=>{
    let id = req.params.id
    try {
        let result= Teacher.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Teacher delete successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
 }