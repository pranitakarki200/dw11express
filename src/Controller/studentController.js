import { Student } from "../schema/model.js"
import { studentSchema } from "../schema/studentschema.js"



export let createStudent= async(req,res,next)=>{
    let data = req.body
    try {
    
      let result = await Student.create(data)
    res.json({
      success:true,
      message:"Students Create Successfully",
      result:result
      
    })
      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
      
    }
   


  }

  export let getStudent=async(req,res,next)=>{
      
    try {

      //number searching
      // let result = await Student.find({})
        // let result = await Student.find({name:"ramu"})
        //While searching we only focus on value (we don't focus on type)
        // let result = await Student.find({name:"sita", roll: 57})
        // let result = await Student.find( {roll:50 } ) //roll=50
        // let result = await Student.find({roll:{$gt:50}})

        // let result = await Student.find( {roll: {$gt:50} } ) //greater than 50
        // let result = await Student.find({roll:{$gte:50}}) //greater than equal to 50
        //let result = await Student.find({roll:{ $lt:50} }) //less than 50
        // let result = await Student.find({roll:{ $lte:50} }) //less than equal to 50
        // let result = await Student.find({roll:{ $ne:50 } }) //not equal to 50
        // let result = await Student .find ({roll:{ $in:[20, 25, 30]}}) //include 20, 25,30
        //let result = await student.find({roll:{ $gte:20,$lte:25}})

        //string searching
        //let result = await student.find({name:{$in:["Pranita","ram"]}})
        //let result = await

        // .find({name:"nitan"})
// .find({name:/nitan/})
// .find({name:/nitan/i})
// .find({name:/ni/})
// .find({name:/ni/i})
// / .find({name:/^ni/})
// .find({name:/^ni/i})
// .find({name:/ni$/})

//select
//find has control over the object where asa select as control over the object properties
//let result = await Student.find({}).select("name gender -_id")
//let result = await student.find({}).select("-name -gender")
                                      //.select(" name -gender ")//it is not valid 
      //in select use either all - or use all + but do not use both except _id   
      
      //for sorting

      //.find ({}).sort("name")
      //.find ({}).sort("-name")
       //                ("name age")
       //                 ("name -age")
       //                  ("-name age")
       //                   ("age -name")

       //skip
       //let result = await student.find({})

       //limit
       //

       //this order this 
       //find ,sort,select,skip,limit

      res.json({
        success:true,
        message:"Student Read Successfully",
        result:result
      })
      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
      
    }

    
  
   
  }

  export let getSpecificStudent=async(req,res,next,)=>{
    let id = req.params.id
    try {
      let result= await Student.findById(id)
      //if {new:female} it give old data 
      //if {new;true} it give new data 
      res.json({
        success:true,
        message:"Student Read successfully",
        result:result
      })

      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
      
      
    }
    
    
    
    
      
    }


  export let updateStudent=async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
      
    let result= await Student.findByIdAndUpdate(id,data,{new:true})
    res.json({
      success:true,
      message:"Student updated successfully",
      result:result
    })
      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
      
    }
    
    
   
  }


   export let deleteStudent=async(req,res,next)=>{
    let id = req.params.id
    try {
      let result= await Student.findByIdAndDelete(id)
      if (result===null) {
        res.json({
          success:false,
          message:"Student doesn't exit"
        })
        
      } else {
        res.json({
          success:true,
          message:"Student deleted successfully",
          result:result,
        });
        
      }
     
      
      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
      
    }
    
    
  }