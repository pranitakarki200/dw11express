import Jwt from "jsonwebtoken";

//import { JsonWebTokenError } from "jsonwebtoken";
import { WebUser } from "../schema/model.js";
import bcrypt, { hash } from "bcrypt";
import { secretKey } from "../constant.js";
import { sendEmail } from "../utils/sendMail.js";

// post
export const createWebUser = async (req, res, next) => {
  try {
    let data = req.body;
    let hashPassword = await bcrypt.hash(data.password, 10);
    data = {
      ...data,
      isVerifiedEmail: false,
      password: hashPassword,
    };
    let result = await WebUser.create(data);
    // send email with link
    let infoObj = {
      _id: result._id,
    };

    let expiryInfo = {
      expiresIn: "365d",
    };

    let token = await Jwt.sign(infoObj, secretKey, expiryInfo);

    sendEmail({
      from: "`pranita`<pranitakarki200@gmail.com>",
      to: data.email,
      subject: `Account Activation`,
      html: `
                <div>
                <h1>You have successfully registered in our system.</h1>
                <a href="http:localhost:3000/verify-email?token=${token}">Click Here</a>
                <p>http:localhost:3000/verify-email?token=${token}</p>
                </div>
                `,
    });

    res.json({
      success: true,
      message: "create user successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const verifyEmail = async (req, res, next) => {
  try {
    let token = req.headers.authorization.split(" ")[1];
    try {
      let infoObj = Jwt.verify(token, secretKey); //env
      let userId = infoObj._id;
      let result = await WebUser.findByIdAndUpdate(
        userId,
        { isVerifiedEmail: true },
        { new: true }
      );
      res.json({
        success: true,
        message: "webUser verified successfully",
        data: result,
      });
    } catch (error) {
      console.log(error.message);
    }
  } catch (error) {
    res.json({
      success: false,
      message: "failed to get data",
    });
  }
};
export const loginUser = async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;
    // console.log(`${email}+${password}`);
    let user = await WebUser.findOne({ email: email });
    if (user) {
      if (user.isVerifiedEmail) {
        let isValidPassword = await bcrypt.compare(password, user.password);
        if (isValidPassword) {
          let infoObj = {
            _id: user._id,
          };
          let expiryInfo = {
            expiresIn: "365d",
          };
          let token = Jwt.sign(infoObj, secretKey, expiryInfo);
          res.json({
            success: true,
            message: "User Login Successfully",
            data: token,
          });
        } else {
          let error = new Error("Credentials doesn't match");
          throw error;
        }
      } else {
        let error = new Error("Credentials not found.1");
        throw error;
      }
    } else {
      let error = new Error("Credentials not found.2");
      throw error;
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const myProfile = async (req, res, next) => {
  try {
    let _id = req._id
    let result = await WebUser.findById(_id)
    res.json ({
      success: true,
      message:"profile read successfully"
    })
    res.json("888888888888888888888");
  } catch (error) {
    res.json({
      success: false,
      message: "unable to read profile",
    });
  }
};
export const updateProfile = async(req,res)=>{
  try {
    let _id = req._id
    let data = re.body 
    let result = await WebUser.findByIdAndUpdate(_id,data,{new:true})
    res.json({
      success:true,
      message : "profile update successfully",
    })
  } catch (error) {
    
  }
}

// getAll
export let readWebUserAll = async (req, res, next) => {
  try {
    let result = await WebUser.find({});
    res.json({
      success: true,
      message: "webUser get successfully",
      data: result,
    });
  } catch {
    res.json({
      success: false,
      message: "failed to get data",
    });
  }
};
// getSpecific
export let readWebUserSpecific = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await WebUser.findById(id);
    res.json({
      success: true,
      message: "Data found successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
// patch
export let updateWebUser = async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;
  try {
    let result = await WebUser.findByIdAndUpdate(id, data);
    res.json({
      success: true,
      message: "Data updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebUser = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await WebUser.findByIdAndDelete(id);
    res.json({
      success: true,
      message: "Data deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};