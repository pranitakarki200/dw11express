import { Book } from "../schema/model.js"


 export let createBook=async(req,res,next)=>{
    let data=req.body
    try {
        let result =await Book.create(data)
        res.json({
            success: true,
            message:"Book create successfully",
            result:result,

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
    

    
}


export let readBook=async(req,res,next)=>{

    let data=req.body
    try {
        let result= await Book.find({})
res.json({
    success:true,
    message:"Book Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let readSpecificBook=async(req,res,next)=>{



    let data=req.body
    try {
        let result= await Book.findById(id)
res.json({
    success:true,
    message:"Book Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let UpdateBook=async(req,res,next)=>{

    let id =req.params.id


    let data=req.body
    try {
        let result= await Book.findByIdAndUpdate(id,data,{new:true})
res.json({
    success:true,
    message:"Book Updated successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

 export let deleteBook = (req,res,next)=>{
    let id = req.params.id
    try {
        let result= Book.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Book delete successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
 }