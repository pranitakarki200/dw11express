import { Class } from "../schema/model.js"

 export let createClassRoom=async(req,res,next)=>{
    let data=req.body
    try {
        let result =await Class.create(data)
        res.json({
            success: true,
            message:"class Room create successfully",
            result:result,

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
    

    
}


export let readClassRoom=async(req,res,next)=>{

    let data=req.body
    try {
        let result= await Class.find({})
res.json({
    success:true,
    message:"class Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let readSpecificClassRoom=async(req,res,next)=>{



    let data=req.body
    try {
        let result= await Class.findById(id)
res.json({
    success:true,
    message:"class Read successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        


    
        
    }
    
}
export let UpdateClassRoom=async(req,res,next)=>{

    let id =req.params.id


    let data=req.body
    try {
        let result= await Class.findByIdAndUpdate(id,data,{new:true})
res.json({
    success:true,
    message:"class Updated successfully",
    result:result,
})
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

 export let deleteClassRoom = (req,res,next)=>{
    let id = req.params.id
    try {
        let result= Class.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"ClassRoom delete successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
 }