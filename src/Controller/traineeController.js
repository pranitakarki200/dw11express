import { Trainee } from "../schema/model.js"

export let createTrainee =async(req,res,next)=>{
    let data = req.body

    try {
        let result = await Trainee.create(data)
        res.json({
            success:true,
            message:"trainees created successfully",
            result:result,

         })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
         })
    }


     
 }

 export let ReadTrainee=async(req,res,next)=>{
    try {
        let result=await Trainee.find({})
        res.json({
            success:true,
            message:"trainees read successfully",
            result:result
         })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
         })
        
    }
    
}

export let readSpecificTrainee=async(req,res,next)=>{

    let id = req.params.id
    
    try {
        let result = await Trainee.findById(id)
        res.json({
            success:true,
            message:"trainee read specific successfully",
            result:result
         })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
         })
        
    }
   
 }

 export let updateTrainee=async(req,res,next)=>{
    let data = req.body
    let id =req.params.id
    try {
        
    let result=await Trainee.findByIdAndUpdate(id,data,{new:true})
    res.json({
        success:true,
        message:'update trainee successful',
        result:result

    })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
    
 }

 export let deleteTrainee=async(req,res,next)=>{
    let id=req.params.id
    try {
        
    let result=await  Trainee .findByIdAndDelete(id)
    if (result===null) {
        res.json({
        
            success:false,
            message:"Trainee doesn't exist",
        
        })
        
    } else {
        res.json({
        
            success:true,
            message:"delete successfully",
            result:result
        })
        
    }
   
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

 }
 