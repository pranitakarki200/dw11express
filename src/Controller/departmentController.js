import { Department } from "../schema/model.js"
export let createDepartment = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await Department.create(data)
        res.json({
            success:true,
            message:"Department created successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let readDepartment = async (req,res,next)=>{
    try {
        let result = await Department.find({})
        res.json({
            success: true,
            message:"Department read successfully",
            results : result,
        })
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
    }
}
export let readSpecificDepartment = async (req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Department.findById(id)
        res.json({
            success: true,
            message: "Department with the given ID found.",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message: "No department with such ID exists."
        })
    }
}
export let updateDepartment = async (req,res,json)=>{
    let data=req.body
    let id = req.params.id
    try {
        let result = await Department.findByIdAndUpdate(id, data,{new:true})
        res.json({
            success:true,
            message:"Department updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export let deleteDepartment = async (req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Department.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"Department doesn't exist"
            })
        } else {
            res.json({
                success:true,
                message:"Deleted Successfully",
                result:result
            })
        }
    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
    }
}