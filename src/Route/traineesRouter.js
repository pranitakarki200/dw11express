import { Router } from "express";
import { ReadTrainee, createTrainee, deleteTrainee, readSpecificTrainee, updateTrainee } from "../Controller/traineeController.js";

export let traineesRouter = Router()

traineesRouter
.route("/")
.post(createTrainee)
.get(ReadTrainee)



traineesRouter
.route("/:id")
.get(readSpecificTrainee)
.patch(updateTrainee)
.delete(deleteTrainee)


