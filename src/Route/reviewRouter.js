
import { Router } from "express"
import { createReview, deleteReview, readReview, readSpecificReview, updateReview } from "../Controller/reviewController.js"

export let reviewRouter = Router()
reviewRouter
.route("/")
.post(createReview)
.get(readReview)
reviewRouter
.route("/:id")
.get(readSpecificReview)
.patch(updateReview)
.delete(deleteReview)