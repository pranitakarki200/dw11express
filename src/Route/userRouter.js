import { Router } from "express"
import { createUser, deleteUser, readUser, readSpecificUser, updateUser } from "../Controller/userController.js"
// import { createUser, deleteUser, readSpecificUser, readUser, updateUser } from "../Controller/userController.js"

export let userRouter = Router()
userRouter
.route("/")
.post(createUser)
.get(readUser)
userRouter
.route("/:id")
.get(readSpecificUser)
.patch(updateUser)
.delete(deleteUser)