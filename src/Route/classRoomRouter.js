import { Router } from "express";
import { UpdateClassRoom, createClassRoom, deleteClassRoom, readClassRoom, readSpecificClassRoom } from "../Controller/classRoomController.js";

export let classRoomRouter=Router()
classRoomRouter
.route("/")
.post( createClassRoom)
.get(readClassRoom)

classRoomRouter
.route("/:id")
.get(readSpecificClassRoom)
.patch(UpdateClassRoom)
.delete(deleteClassRoom)

