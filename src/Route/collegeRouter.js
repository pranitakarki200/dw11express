import { Router } from "express";
import { ReadCollege, createCollege, deleteCollege, readSpecificCollege, updateCollege } from "../Controller/collegeController.js";

export let CollegeRouter = Router()

CollegeRouter
.route("/")
.post(createCollege)
.get(ReadCollege)



CollegeRouter
.route("/:id")
.get(readSpecificCollege)
.patch(updateCollege)
.delete(deleteCollege)


