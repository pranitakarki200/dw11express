import { Router } from "express";
import { UpdateTeacher, createTeacher, deleteTeacher, readSpecificTeacher, readTeacher } from "../Controller/teacherController.js";


export let TeacherRouter=Router()
TeacherRouter
.route("/")
.post( createTeacher)
.get(readTeacher)

TeacherRouter
.route("/:id")
.get(readSpecificTeacher)
.patch(UpdateTeacher)
.delete(deleteTeacher)

