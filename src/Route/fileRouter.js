import { Router } from "express"
import { uploadMultipleFiles, uploadSingleFile } from "../Controller/fileController.js"
import upload from "../utils/fileUpload.js"
// import { createUser, deleteUser, readSpecificUser, readUser, updateUser } from "../Controller/userController.js"

export let fileRouter = Router()
fileRouter
.route("/single")
.post(upload.single("profileImage"),uploadSingleFile)


fileRouter
.route("/multiple")
.post(upload.array("profileImage"), uploadMultipleFiles)