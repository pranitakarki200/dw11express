import { Router } from "express";
import { UpdateBook, createBook, deleteBook, readBook, readSpecificBook } from "../Controller/bookController.js";



export let BookRouter=Router()
BookRouter
.route("/")
.post( createBook)
.get(readBook)

BookRouter
.route("/:id")
.get(readSpecificBook)
.patch(UpdateBook)
.delete(deleteBook)

