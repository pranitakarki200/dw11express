import { Router } from "express"
import { createWebUser, deleteWebUser, loginUser, myProfile, readWebUserAll, readWebUserSpecific, updateProfile, updateWebUser, verifyEmail } from "../Controller/webUserController.js"
import { secretKey } from "../constant.js";
import jwt from "jsonwebtoken"
import isAuthenticated from "../middleware/isAuthenticated.js";

// import { verify } from "jsonwebtoken"

// import { createWebUser, deleteWebUser, readSpecificWebUser, readWebUser, updateWebUser } from "../Controller/WebUserController.js"

 export let WebUserRouter = Router()
 WebUserRouter
 .route("/")
 .post(createWebUser)
 .get(readWebUserAll)
WebUserRouter.route("/verify-email").patch(verifyEmail);
WebUserRouter.route("/login").post(loginUser);
WebUserRouter.route("/my-profile").get( isAuthenticated,myProfile)
WebUserRouter.route("/update-profile").get( isAuthenticated,updateProfile)


 

