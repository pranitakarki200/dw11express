import { config } from "dotenv";

config()
export let port = process.env.PORT
export let mongoUrl = process.env.MONGO_URL
export let secretKey = process.env.SECRET_KEY
export let user = process.env.user
export let pass = process.env.PASSpu