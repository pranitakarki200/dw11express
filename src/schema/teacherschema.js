import { Schema } from "mongoose";


export let teacherSchema =Schema({
     fieldName:{
        type:String,
        required:[true, "fieldName is required"],
    },
 age:{
    type:Number,
    required:[true, "age field is required"],
 },

 isMarried:{
    type:Boolean,
    required:[true,"isMarried field is required"],
 },
 subject:{
    type:String,
    required:[true,"subject field is required"],

 },

 
})