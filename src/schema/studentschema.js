import { Schema } from "mongoose";


export let studentSchema = Schema({

    name :{
        type:String,
        required:[true,"name field is required"],
        // lowercase: true,
        // uppercase: true,
        // trim:true,

        minLength:[3,"name must be at least 3 character long."],
        maxLength:[8,"name must be at most 8 character long"],

        validate:(value)=>{
            let onlyAlphabet=/^[a-zA-Z]+$/.test(value)
            if(onlyAlphabet){

            }else{
                throw new Error("name field must have only alphabet")
            }

        }
    },
    phoneNumber:{
        type:Number,
        required:[true,"PhoneNumber field is required."],
        validate: (value)=>{
           let strString = String(value)

           if(strString.length===10)
           {

           }
           else{
            throw new Error("phoneNumber must be exact 10 character long.")
           }
        }

        
     
    },
    
    gender:{
        type:String,
        default:"female",
        required:[true,"gender field is required"],
        validate: (value)=>{
            if(value==="male" || value ==="female"|| value ==="other"){

            }else{
                let error = new Error("gender must be either male,female,other")
            }
        }
        
    },

    password:{
        type:String,
        required:[true,"password field is required"],

        // password must have 1 number, one lower case, one uppercase , one symbol, min 8 character, max 15 character.

        validate:(value)=>{
            let validatePassword =(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/).test(value)
            if (validatePassword) {
                
            } else {
                throw new Error("password must have 1 number,one Lower case, one uppercase,one symbol,min 8 character, max 15 character")
                
            }
        }
    },
    roll:{
        type:Number,
        required:[true,"roll field is required"],
        min:[50,"roll must be greater than or equal 50"],
        max:[100,"roll must be less than or equal than 100"],
    },
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required"],
    },
    spouseName:{
        type:String,
        required:[true,"spouseName field is required"],

    },
    email:{
        type:String,
        required:[true,"email field is required"],
        unique:true,
        validate:(value)=>{
            let isValidEmail=/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
    

            if(isValidEmail)
            {

            }else{
                throw new Error("email must be valid")
            }
        }
    },
    dob:{
        type:Date,
        required:[true,"dob field is required"],
    },
    
    location:{
        country:{
            type:String,
            required:["true","country fields is required"],
        },
        exactLocation:{
            type:String,
            required:[true,"exactLocation field is required"],
        }
        
    },
    favTeacher:[
        {
            type:String,
        },
    ],
    favSubject:[
       {
bookName:{
    type:String,
    required:[true,"bookName is required"],
},
bookAuthor:{
    type:String,
    required:[true,"bookAuthor is required"],
}
       },


        
    ],

  

       
    

    })

    