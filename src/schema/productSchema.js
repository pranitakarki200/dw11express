import { Schema } from "mongoose"
export let productSchema = Schema({
    name:{
        type: String,
        required:[true,"Please provide your  Name"]
    },
    price:{
        type: Number,
        required: [true,"price is needed"]
    },
    quantity:{
        type:Number,
        required:[true, "quantity is required"]
    },
  
})
// data :{
//     fullName:"nitan",
//     address:"gagalphedi",
//     phoneNumber:9849468999,
//     email:"nitanthapa425@gmail.com"
// }