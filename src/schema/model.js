import { model } from "mongoose";

import { teacherSchema } from "./teacherschema.js";
import { traineeSchema } from "./traineeSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { departmentSchema } from "./DepartmentSchema.js";

import { studentSchema } from "./studentschema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { bookSchema } from "./bookSchema.js";
import { contactSchema } from "./contactSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";
import webUserSchema from "./webUserSchema.js";


// export let Student = model("Student",studentSchema)
export let Teacher = model("Teacher",teacherSchema)

export let Trainee= model("Trainee",traineeSchema)
 export let College = model("College",collegeSchema)
  export let Class = model("Class",classRoomSchema)
 export let Department=model("Department",departmentSchema)
 export let Student=model("Student",studentSchema)
 export let Book=model("Book",bookSchema)
 export let Contact=model("Contact",contactSchema)
 export let Product=model("Product",productSchema)
 export let User=model("User",userSchema)
 export let Review=model("Review",reviewSchema)
 export let WebUser =model("WebUser",webUserSchema)
 // always capital
 
 
 

 
