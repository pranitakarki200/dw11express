import { Schema } from "mongoose";

export let traineeSchema = Schema({
    name : {
        type:String,
        required:[true,"Name field is required"],

    },
    class:{
        type:Number,
        required:[true,"Class field is required"],
    },
    Faculty:{

        type:String,
        required:[true,"Faculty field is required"],
    },
    
})
