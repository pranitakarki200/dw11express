import { Schema } from "mongoose";


export let bookSchema =Schema({
      Name:{
        type:String,
        required:[true, "Name is required"],
    },
 author:{
    type:String,
    required:[true, "author field is required"],
 },

 Price:{
    type:Number,
    required:[true," price field is required"],
 },
 isAvailable:{
    type:Boolean,
    required:[true," isAvailable field is required"],

 },

 
})