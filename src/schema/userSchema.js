import { Schema } from "mongoose";
//user
// name
// address
// phonenumber
// email
// password
export let userSchema = Schema({
    profileImage:{
        type: String,
        required: true
    },
    address: {
        type:String,
        required: [true, "address field is required"],
    },
    phoneNumber: {
        type:Number,
        required: [true, "phoneNumber field is required"],
    },
    email: {
        type:String,
        required: [true, "email field is required"],
        unique:true,
    },
    password: {
        type:String,
        required: [true, "password field is required"],
    },
    
},{
    timestamps: true  // Saves createdAt and updatedAt as dates. 
})