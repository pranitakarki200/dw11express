import mongoose from "mongoose"
import { mongoUrl } from "../constant.js"

export let connectToMongoDb =() =>{
  mongoose.connect  (mongoUrl)
}